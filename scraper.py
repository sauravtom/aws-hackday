from bs4 import BeautifulSoup
import requests
import json
import pdb
import os
import random
import argparse
import pyrebase
import re
import hashlib

DIR_PATH = os.path.dirname(os.path.realpath(__file__))

config = {
    "apiKey": "AIzaSyCiAHXUQqAzV_n3JOALs6ONLhjn2tffLR4",
    "authDomain": "uberdelhi.firebaseapp.com",
    "databaseURL": "https://uberdelhi.firebaseio.com",
    "projectId": "uberdelhi",
    "storageBucket": "uberdelhi.appspot.com",
    "messagingSenderId": "148518018364",
    "serviceAccount": "%s/firebase_secret.json"%(DIR_PATH)
}

firebase = pyrebase.initialize_app(config)
db = firebase.database()

def clean_str(s):
	s=s.strip()
	return re.sub(r'[^\x00-\x7F]+',' ', s)

def replace_branding(s):
	s=s.replace('?utm_source=icobench','')
	return s

def icobench_scraper():
	result_arr=[]
	temp_arr =[]
	for page in range(1,308):
		url = "https://icobench.com/icos?page=%s"%page
		html = requests.get(url).text
		soup = BeautifulSoup(html,'lxml')
		for i in soup.select('td.ico_data'):
			d={}
			temp_link = i.select('a.name')[0].get('href')
			d['link'] = "https://icobench.com"+temp_link
			print temp_link
			temp_image = i.select('a.image')[0].get('style').split("('")[-1].split("')")[0]
			d['image'] = "https://icobench.com/images%s"%temp_image
			d['short_description'] = i.select('div.content')[0].select('p')[0].get_text()
			temp_arr.append(d)

	for ico in temp_arr:
		try:
			headers = {'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/39.0.2171.95 Safari/537.36'}
			html = requests.get(ico['link'],headers=headers).text
			soup = BeautifulSoup(html,'lxml')

			title = soup.select('div.name')[0].select('h1')[0].get_text()
			title = clean_str(title)
			print title
			item={}
			item['image'] = ico['image']
			item['temp_link'] = ico['link']
			item['short_description_'] = ico['short_description']
			item['title'] = title
			item['pitch'] = soup.select('div.name')[0].select('h2')[0].get_text()
			item['short_description'] = soup.select('div.ico_information')[0].select('p')[0].get_text()
			item['categories'] = [i.get_text() for i in soup.select('div.categories')[0].select('a')]
			item['rating'] = soup.select('div.rate')[0].get_text()
			item['big_description'] = soup.select('div.tab_content')[0].get_text()
			item['whitepaper'] = soup.select('div.tabs')[0].select('a')[-1].get('href')

			social_arr = [x.get('href') for x in soup.select('div.socials')[0].select('a')]
			e = {}
			e['github'] = ""
			e['twitter'] = ""
			e['telegram'] = ""
			e['slack'] = ""
			e['facebook'] = ""
			e['bitcointalk'] = ""

			for sl in social_arr:
				if not sl:
					continue
				if 'slack' in sl:
					e['slack'] = sl
				elif 'github' in sl:
					e['github'] = sl
				elif 'twitter' in sl:
					e['twitter'] = sl
				elif 'facebook' in sl:
					e['facebook'] = sl
				elif 'reddit' in sl:
					e['reddit'] = sl
				elif 't.me' in sl or 'telegram' in sl:
					e['telegram'] = sl
				elif 'bitcointalk' in sl:
					e['bitcointalk'] = sl
				elif 'icobench' in sl:
					ico_link = sl

			item['social_links'] = e
			item['website']= replace_branding(ico_link)
			item['whitepaper_text'] = ''

			result_arr.append(item)
		except:
			pass

	print result_arr
	db.child('aws_hackday').child('x').set(result_arr)


def test():
	pass


parser = argparse.ArgumentParser(description='ICO Scraper v1.0')
parser.add_argument('-r','--run',help='name of function to run',required=True)

args = parser.parse_args()

if args.run == 'scrape':
	icobench_scraper()

elif args.run == 'test':
	test()
